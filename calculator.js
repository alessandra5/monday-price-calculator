// Include the findBestPrice function here or import it if it's in a separate file

document.getElementById('calculate').addEventListener('click', function() {
    // Get the quantities from the form
    const qtyA = parseInt(document.getElementById('quantityA').value, 10);
    const qtyB = parseInt(document.getElementById('quantityB').value, 10);
    const qtyC = parseInt(document.getElementById('quantityC').value, 10);
    const qtyD = parseInt(document.getElementById('quantityD').value, 10);

    // Validate input
    if (isNaN(qtyA) || isNaN(qtyB) || isNaN(qtyC) || isNaN(qtyD)) {
        alert('Please enter valid quantities.');
        return;
    }

    // Calculate the best price
    const bestPriceCalculation = findBestPrice(qtyA, qtyB, qtyC, qtyD);

    // Display the result
    const resultDiv = document.getElementById('result');
    resultDiv.innerHTML = `<p>Optimal combination of products: ${JSON.stringify(bestPriceCalculation.quantities)}</p>`;
    resultDiv.innerHTML += `<p>Total price: $${bestPriceCalculation.totalPrice} ARR</p>`;
});


function findBestPrice(qtyA, qtyB, qtyC, qtyD) {
    // Define the prices for individual products and combinations
    const prices = {
        a: 528, b: 624, c: 576, d: 624,
        ab: 960, ac: 960, ad: 960,
        bc: 960, bd: 960, cd: 960,
        abc: 1440, abd: 1440, acd: 1440,
        bcd: 1440, abcd: 1800
    };

    // Sort combinations by unit price
    const combos = ['abcd', 'abc', 'abd', 'acd', 'bcd', 'ab', 'ac', 'ad', 'bc', 'bd', 'cd'];
    combos.sort((x, y) => (prices[x] / x.length) - (prices[y] / y.length));

    // Object to hold the result
    let result = {
        quantities: {
            a: 0, b: 0, c: 0, d: 0,
            ab: 0, ac: 0, ad: 0,
            bc: 0, bd: 0, cd: 0,
            abc: 0, abd: 0, acd: 0,
            bcd: 0, abcd: 0
        },
        totalPrice: 0
    };

    // Helper function to update quantities and calculate the price
    const updateQuantitiesAndPrice = (combo) => {
        combo.split('').forEach(product => {
            result.quantities[product]--;
        });
        result.quantities[combo]++;
        result.totalPrice += prices[combo];
    };

    // Greedy algorithm to find the best combination
    for (let combo of combos) {
        while (qtyA - (combo.includes('a') ? 1 : 0) >= 0 &&
               qtyB - (combo.includes('b') ? 1 : 0) >= 0 &&
               qtyC - (combo.includes('c') ? 1 : 0) >= 0 &&
               qtyD - (combo.includes('d') ? 1 : 0) >= 0) {
            updateQuantitiesAndPrice(combo);
            qtyA -= combo.includes('a') ? 1 : 0;
            qtyB -= combo.includes('b') ? 1 : 0;
            qtyC -= combo.includes('c') ? 1 : 0;
            qtyD -= combo.includes('d') ? 1 : 0;
        }
    }

    // Add individual products if any quantity is left
    ['a', 'b', 'c', 'd'].forEach(product => {
        while (eval('qty' + product.toUpperCase()) > 0) {
            result.quantities[product]++;
            result.totalPrice += prices[product];
            eval('qty' + product.toUpperCase() + '--');
        }
    });

    return result;
}

// Example usage:
//const bestPriceCalculation = findBestPrice(20, 10, 15, 5);
//console.log(bestPriceCalculation);
